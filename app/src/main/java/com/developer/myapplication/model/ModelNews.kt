package com.developer.myapplication.model

import com.developer.myapplication.app.MyApp
import io.realm.Case
import io.realm.Realm
import io.realm.RealmResults
import io.realm.Sort
import javax.inject.Inject

class ModelNews : IModelNews {
    @Inject
    lateinit var realm: Realm

    @Inject
    lateinit var modelChanel: IModelChanel

    init {
        MyApp.getComponent().inject(this)
    }

    override fun getAll(): RealmResults<DbNews>? {
        return realm.where(DbNews::class.java).findAll()
    }

    override fun getNewsFromChanel(chanelId: Long): RealmResults<DbNews>? {
        return realm.where(DbNews::class.java).equalTo("chanel.id", chanelId).findAll()
    }

    override fun findById(id: Long): DbNews? {
        return realm.where(DbNews::class.java).equalTo("id", id).findFirst()
    }

    override fun createRecord(
        title: String,
        description: String,
        link: String,
        pubDate: String,
        idChanel: Long
    ) {

        val realmThread = Realm.getDefaultInstance()

        val num = realmThread.where<DbNews>(DbNews::class.java).max("id")
        val nextID: Long
        if (num == null) {
            nextID = 1
        } else {
            nextID = num.toLong() + 1
        }

        realmThread.beginTransaction()
        val db = DbNews()
        db.id = nextID
        db.title = title
        db.description = description
        db.link = link
        db.pubDate = pubDate

        val chanel = modelChanel.findById(idChanel)
        if(chanel!=null) {
            db.chanel = chanel
        }
        realmThread.insertOrUpdate(db)
        realmThread.commitTransaction()
    }

    override fun findNews(search: String): RealmResults<DbNews>? {

        return realm.where(DbNews::class.java!!)
            .like(
                "title",
                "*" + search + "*",
                Case.INSENSITIVE
            ).findAll()
    }

    override fun clearAll() {
        realm.beginTransaction()
        getAll()!!.deleteAllFromRealm()
        realm.commitTransaction()
    }
}