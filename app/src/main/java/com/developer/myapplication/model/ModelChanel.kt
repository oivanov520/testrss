package com.developer.myapplication.model

import com.developer.myapplication.app.MyApp
import io.realm.Realm
import io.realm.RealmResults
import javax.inject.Inject

class ModelChanel : IModelChanel {
    @Inject
    lateinit var realm: Realm

    init {
        MyApp.getComponent().inject(this)
    }

    override fun getAll(): RealmResults<DbChanel>? {
        return realm.where(DbChanel::class.java).findAll()
    }

    override fun findById(id: Long): DbChanel? {
        return Realm.getDefaultInstance().where(DbChanel::class.java).equalTo("id", id).findFirst()
    }

    override fun findByLink(link: String): DbChanel? {
        return Realm.getDefaultInstance().where(DbChanel::class.java).equalTo("link", link).findFirst()
    }

    override fun createRecord(link: String) {
        val num = realm.where<DbChanel>(DbChanel::class.java).max("id")
        val nextID: Long
        if (num == null) {
            nextID = 1
        } else {
            nextID = num.toLong() + 1
        }

        realm.beginTransaction()
        val db = DbChanel()
        db.id = nextID
        db.link = link
        realm.insertOrUpdate(db)
        realm.commitTransaction()
    }

    override fun clearAll() {
        realm.beginTransaction()
        getAll()!!.deleteAllFromRealm()
        realm.commitTransaction()
    }
}