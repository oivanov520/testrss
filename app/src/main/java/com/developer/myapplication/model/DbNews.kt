package com.developer.myapplication.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmModel
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class DbNews : RealmModel {
    @PrimaryKey
    @SerializedName("id")
    @Expose
    var id: Long? = 0

    @SerializedName("title")
    @Expose
    var title: String? = ""

    @SerializedName("description")
    @Expose
    var description: String? = ""

    @SerializedName("link")
    @Expose
    var link: String? = ""

    @SerializedName("pubDate")
    @Expose
    var pubDate: String? = ""

    @SerializedName("chanel")
    @Expose
    var chanel: DbChanel? = DbChanel()
}