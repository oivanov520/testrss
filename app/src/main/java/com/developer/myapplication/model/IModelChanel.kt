package com.developer.myapplication.model

import io.realm.RealmResults

interface IModelChanel {
    fun getAll(): RealmResults<DbChanel>?
    fun findById(checkId: Long): DbChanel?
    fun createRecord(link: String)
    fun clearAll()
    fun findByLink(link: String): DbChanel?
}