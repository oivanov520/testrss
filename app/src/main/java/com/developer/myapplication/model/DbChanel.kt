package com.developer.myapplication.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmModel
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class DbChanel : RealmModel {
    @PrimaryKey
    @SerializedName("id")
    @Expose
    var id: Long? = 0

    @SerializedName("link")
    @Expose
    var link: String? = ""
}