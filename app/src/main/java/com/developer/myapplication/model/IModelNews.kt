package com.developer.myapplication.model

import io.realm.RealmResults

interface IModelNews {
    fun getAll(): RealmResults<DbNews>?
    fun getNewsFromChanel(checkId: Long): RealmResults<DbNews>?
    fun findById(checkId: Long): DbNews?
    fun createRecord(title: String, description: String, link: String, pubDate: String, idChanel: Long)
    fun findNews(search: String): RealmResults<DbNews>?
    fun clearAll()
}