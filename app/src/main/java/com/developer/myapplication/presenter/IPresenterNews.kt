package com.developer.myapplication.presenter

import com.developer.myapplication.model.DbChanel
import com.developer.myapplication.model.DbNews
import com.developer.myapplication.util.IListenerNetwork
import io.realm.RealmResults

interface IPresenterNews {
    fun addChanel(link: String)
    fun downloadAllNews(listener: IListenerNetwork)
    fun getChanelList(): RealmResults<DbChanel>?
    fun getNewsFromChanel(search: String): RealmResults<DbNews>?
    fun clearDataBase()
}