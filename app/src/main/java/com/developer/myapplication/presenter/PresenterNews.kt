package com.developer.myapplication.presenter

import android.util.Log
import com.developer.myapplication.app.MyApp
import com.developer.myapplication.model.DbChanel
import com.developer.myapplication.model.DbNews
import com.developer.myapplication.model.IModelChanel
import com.developer.myapplication.model.IModelNews
import com.developer.myapplication.util.IListenerNetwork
import com.developer.myapplication.util.INetworkNewsManager
import io.realm.RealmResults
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

class PresenterNews : IPresenterNews {

    @Inject
    lateinit var modelChanel: IModelChanel

    @Inject
    lateinit var modelNews: IModelNews

    @Inject
    lateinit var networkNewsManager: INetworkNewsManager

    val commonPool = GlobalScope.coroutineContext

    init {
        MyApp.getComponent().inject(this)
    }

    override fun addChanel(link: String) {
        modelChanel.createRecord(link)
    }

    //+coroutines
    override fun downloadAllNews(listener: IListenerNetwork) {
        startDownload(listener)
    }

    fun startDownload(listener: IListenerNetwork)= runBlocking(commonPool) {
        val job = launch(commonPool) {
            downloadNews(listener)
        }
        job.join()
    }

    suspend fun downloadNews(listener: IListenerNetwork){
        val allChanel = modelChanel.getAll()
        for(el in allChanel!!){
            val res = downloadOneChanel(el.link!!)
            Log.i("TESTER link ", el.link)
            Log.i("TESTER res", res)
        }
        listener.onChange("OK", true)
    }

    suspend fun downloadOneChanel(link: String): String{
        val res =  GlobalScope.async(commonPool) {
            networkNewsManager.downloadNews(link)
        }

        return res.await()
    }
    //-coroutines

    override fun getChanelList(): RealmResults<DbChanel>? {
        return modelChanel.getAll()
    }

    override fun getNewsFromChanel(search: String): RealmResults<DbNews>? {
        if(search.isNullOrEmpty()){
            return modelNews.getAll()
        }else{
            return modelNews.findNews(search)
        }
    }

    override fun clearDataBase(){
        modelNews.clearAll()
        modelChanel.clearAll()
    }
}