package com.developer.myapplication.ui

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.developer.myapplication.R
import com.developer.myapplication.app.MyApp
import com.developer.myapplication.presenter.IPresenterNews
import com.developer.myapplication.ui.adapter.AdapterChanel
import com.developer.myapplication.ui.adapter.AdapterNews
import com.developer.myapplication.util.IListenerNetwork
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var presenterNews: IPresenterNews

    private var adapterChanel: RecyclerView.Adapter<*>? = null

    private var adapterNews: RecyclerView.Adapter<*>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        MyApp.getComponent().inject(this)

        btnStep1.setOnClickListener {
            if(presenterNews.getChanelList()!!.size>0){
                Toast.makeText(this@MainActivity, "already initialized", Toast.LENGTH_SHORT).show()
            }else {
                presenterNews.addChanel("https://news.tut.by/rss/index.rss")
                presenterNews.addChanel("https://news.tut.by/rss/society.rss")
                presenterNews.addChanel("https://news.tut.by/rss/it.rss")
            }
            refreshLinkUI()
        }

        btnStep2.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            presenterNews.downloadAllNews(object : IListenerNetwork{
                override fun onChange(obj: Any, status: Boolean) {
                    Log.i("TESTER", "COMPLITED!")
                    refreshNewsUI()
                }
            })
        }

        btnClear.setOnClickListener {
            presenterNews.clearDataBase()
            refreshLinkUI()
            refreshNewsUI()
        }

        refreshLinkUI()
        refreshNewsUI()
    }

    override fun onResume() {
        super.onResume()
    }

    private fun refreshLinkUI() {

        rvChanel!!.post {
            rvChanel!!.setLayoutManager(LinearLayoutManager(this@MainActivity))
            adapterChanel = AdapterChanel(
                presenterNews.getChanelList()!!,
                this
            )
            rvChanel!!.setAdapter(adapterChanel)
            adapterChanel!!.notifyDataSetChanged()
        }
    }

    private fun refreshNewsUI() {

        rvNews!!.post {
            progressBar.visibility = View.GONE
            rvNews!!.setLayoutManager(LinearLayoutManager(this@MainActivity))
            adapterNews = AdapterNews(
                presenterNews.getNewsFromChanel("")!!,
                this
            )
            rvNews!!.setAdapter(adapterNews)
            adapterNews!!.notifyDataSetChanged()
        }
    }
}
