package com.developer.myapplication.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.developer.myapplication.R
import com.developer.myapplication.model.DbChanel

class AdapterChanel : RecyclerView.Adapter<AdapterChanel.MessageViewHolder> {

    internal var dbChanel: List<DbChanel>
    internal var context: Context

    constructor(data: List<DbChanel>, context: Context) {
        this.dbChanel = data
        this.context = context
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MessageViewHolder {
        val v = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_chanel, viewGroup, false)
        return MessageViewHolder(v)
    }

    override fun onBindViewHolder(viewHolder: MessageViewHolder, i: Int) {
        viewHolder.tvName.text = dbChanel.get(i).link
    }

    override fun getItemCount(): Int {
        return dbChanel.size
    }

    class MessageViewHolder(internal var mView: View) : RecyclerView.ViewHolder(mView) {
        internal var tvName: TextView

        init {
            tvName = mView.findViewById<TextView>(R.id.tvName) as TextView
        }
    }
}