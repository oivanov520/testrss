package com.developer.myapplication.ui.adapter

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.developer.myapplication.R
import com.developer.myapplication.model.DbNews

class AdapterNews : RecyclerView.Adapter<AdapterNews.MessageViewHolder> {

    internal var dbNews: List<DbNews>
    internal var context: Context

    constructor(data: List<DbNews>, context: Context) {
        this.dbNews = data
        this.context = context
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MessageViewHolder {
        val v = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_news, viewGroup, false)
        return MessageViewHolder(v)
    }

    override fun onBindViewHolder(viewHolder: MessageViewHolder, i: Int) {
        viewHolder.tvChanel.text = dbNews.get(i).chanel!!.link
        viewHolder.tvTitle.text = dbNews.get(i).title
        viewHolder.tvDescription.text = dbNews.get(i).description

        viewHolder.llMain.setOnClickListener {
            context.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(dbNews.get(i).link)
                )
            )
        }

    }

    override fun getItemCount(): Int {
        return dbNews.size
    }

    class MessageViewHolder(internal var mView: View) : RecyclerView.ViewHolder(mView) {
        internal var tvChanel: TextView
        internal var tvTitle: TextView
        internal var tvDescription: TextView
        internal var llMain: LinearLayout

        init {
            tvChanel = mView.findViewById<TextView>(R.id.tvChanel) as TextView
            tvTitle = mView.findViewById<TextView>(R.id.tvTitle) as TextView
            tvDescription = mView.findViewById<TextView>(R.id.tvDescription) as TextView
            llMain = mView.findViewById<LinearLayout>(R.id.llMain) as LinearLayout
        }
    }
}