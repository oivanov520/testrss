package com.developer.myapplication.app

import com.developer.myapplication.model.ModelChanel
import com.developer.myapplication.model.ModelNews
import com.developer.myapplication.presenter.PresenterNews
import com.developer.myapplication.ui.MainActivity
import com.developer.myapplication.util.NetworkNewsManager
import dagger.Component
import javax.inject.Singleton

@Component(modules = arrayOf(AppModule::class))
@Singleton
interface AppComponent {
    fun inject(param: MainActivity)
    fun inject(param: ModelChanel)
    fun inject(modelNews: ModelNews)
    fun inject(networkNewsManager: NetworkNewsManager)
    fun inject(presenterNews: PresenterNews)
}