package com.developer.myapplication.app

import android.content.Context
import com.developer.myapplication.model.IModelChanel
import com.developer.myapplication.model.IModelNews
import com.developer.myapplication.model.ModelChanel
import com.developer.myapplication.model.ModelNews
import com.developer.myapplication.presenter.IPresenterNews
import com.developer.myapplication.presenter.PresenterNews
import com.developer.myapplication.util.INetworkNewsManager
import com.developer.myapplication.util.NetworkNewsManager
import dagger.Module
import dagger.Provides
import io.realm.Realm
import io.realm.RealmConfiguration
import java.lang.ref.WeakReference
import javax.inject.Singleton

@Module
internal class AppModule(context: Context) {

    private val realm: Realm

    init {
        Realm.init(context)
        val config = RealmConfiguration.Builder()
            .deleteRealmIfMigrationNeeded()
            .build()
        Realm.setDefaultConfiguration(config)
        this.realm = Realm.getDefaultInstance()
    }

    @Provides
    @Singleton
    fun getRealm(): Realm {
        return realm
    }

    @Provides
    @Singleton
    fun getModelChanel(): IModelChanel {
        return ModelChanel()
    }

    @Provides
    @Singleton
    fun getModelNews(): IModelNews {
        return ModelNews()
    }

    @Provides
    @Singleton
    fun getNetworkNewsManager(): INetworkNewsManager {
        return NetworkNewsManager()
    }

    @Provides
    @Singleton
    fun getPresenterNews(): IPresenterNews {
        return PresenterNews()
    }

}