package com.developer.myapplication.util

interface INetworkNewsManager {
    fun downloadNews(link: String) : String
}
