package com.developer.myapplication.util

interface IListenerNetwork {
    abstract fun onChange(obj: Any, status: Boolean = true)
}