package com.developer.myapplication.util

import com.developer.myapplication.app.MyApp
import com.developer.myapplication.model.IModelChanel
import com.developer.myapplication.model.IModelNews
import okhttp3.OkHttpClient
import okhttp3.Request
import org.w3c.dom.Element
import javax.inject.Inject
import javax.xml.parsers.DocumentBuilderFactory


class NetworkNewsManager : INetworkNewsManager {

    @Inject
    lateinit var modelNews: IModelNews

    @Inject
    lateinit var modelChanel: IModelChanel

    init {
        MyApp.getComponent().inject(this)
    }

    override fun downloadNews(link: String): String {
        val client = OkHttpClient()

        val idChanel = modelChanel.findByLink(link)!!.id

        val request = Request.Builder()
            .header("Accept", "application/json")
            .header("Content-Type", "application/json")
            .url(link)
            .build()

        val res = client.newCall(request).execute()

        val iStream = res.body()?.byteStream()

        //DocumentBuilderFactory, DocumentBuilder are used for
        //xml parsing
        val dbf = DocumentBuilderFactory
            .newInstance()
        val db = dbf.newDocumentBuilder()

        //using db (Document Builder) parse xml data and assign
        //it to Element
        val document = db.parse(iStream)
        val element = document.getDocumentElement()

        //take rss nodes to NodeList
        val nodeList = element.getElementsByTagName("item")

        if (nodeList.length > 0) {
            for (i in 0 until nodeList.length) {

                val entry = nodeList.item(i) as Element

                val _titleE = entry.getElementsByTagName(
                    "title"
                ).item(0) as Element
                val _descriptionE = entry
                    .getElementsByTagName("description").item(0) as Element
                val _pubDateE = entry
                    .getElementsByTagName("pubDate").item(0) as Element
                val _linkE = entry.getElementsByTagName(
                    "link"
                ).item(0) as Element

                val _title = _titleE.getFirstChild().getNodeValue()
                val _description = _descriptionE.getFirstChild().getNodeValue()
                val _pubDate = _pubDateE.getFirstChild().getNodeValue()
                val _link = _linkE.getFirstChild().getNodeValue()


                modelNews.createRecord(_title, _description, _link, _pubDate, idChanel!!)

                //rssItems.add(rssItem)
            }
        }

        return res.body().toString()
    }
}